const express = require("express");

const app = express();

const session = require ( 'express-session' ) ;

const path = require("path");

const sqlite3 = require("sqlite3").verbose();

const db_name = path.join(__dirname, "data", "datawebobjet.db");

var bodyParser = require('body-parser');

var ws = require("nodejs-websocket");

var message = '';
var message_login='';
var user=null;
var err_date =null;
var user_disp =null, user_info=null;
var comp_exist =0 ;
var comp_cherch =null;

const router = express.Router();

/*Connexion à la bd*/
const db = new sqlite3.Database(db_name, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Connexion réussie à la base de données 'datawebobjet.db'");
});

/*Création des tables si elles n'existent pas*/
const sql_create_U = `CREATE TABLE IF NOT EXISTS Utilisateur (
  id_utilisateur INTEGER PRIMARY KEY AUTOINCREMENT,
  nom string NOT NULL,
  prenom string NOT NULL,
  email string NOT NULL,
  tel long NOT NULL,
  mdp string NOT NULL,
  role string NOT NULL
);`;

db.run(sql_create_U, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Création réussie de la table 'Utilisateur'");
});

const sql_create_V = `CREATE TABLE IF NOT EXISTS Ville (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nom string NOT NULL
);`;

db.run(sql_create_V, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Création réussie de la table 'Ville'");
});


const sql_create_Do = `CREATE TABLE IF NOT EXISTS Domaine (
  id_domaine INTEGER PRIMARY KEY AUTOINCREMENT,
  nom_domaine string NOT NULL,
  description string NOT NULL
);`;

db.run(sql_create_Do, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Création réussie de la table 'Domaine'");
});

const sql_create_C = `CREATE TABLE IF NOT EXISTS Competences (
  id_comp INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  id_domaine INTEGER NOT NULL,
  nom_comp string NOT NULL,
  description string NOT NULL,
  duree_approximative string NOT NULL,
  FOREIGN KEY (id_domaine) REFERENCES Domaine(id_domaine)
);`;

db.run(sql_create_C, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Création réussie de la table 'Competences'");
});

const sql_create_Di = `CREATE TABLE IF NOT EXISTS Disponibilites (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  id_utilisateur INTERGER NOT NULL,
  id_competence INTEGER NOT NULL,
  horaire string NOT NULL,
  date string NOT NULL,
  meteo string NOT NULL,
  id_ville INTEGER NOT NULL,
  status INTEGER DEFAULT 0 ,
  FOREIGN KEY (id_ville) REFERENCES Ville(id),
  FOREIGN KEY (id_utilisateur) REFERENCES Utilisateur(id_utilisateur),
  FOREIGN KEY (id_competence) REFERENCES Competences(id_comp)
);`;

db.run(sql_create_Di, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Création réussie de la table 'Disponibilites'");
});

const sql_create_De = `CREATE TABLE IF NOT EXISTS Demande (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  id_utilisateur INTERGER NOT NULL,
  id_competence INTEGER NOT NULL,
  horaire string NOT NULL,
  date string NOT NULL,
  meteo string NOT NULL,
  id_ville INTEGER NOT NULL,
  FOREIGN KEY (id_ville) REFERENCES Ville(id),
  FOREIGN KEY (id_utilisateur) REFERENCES Utilisateur(id_utilisateur),
  FOREIGN KEY (id_competence) REFERENCES Competences(id_comp)
);`;

db.run(sql_create_De, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Création réussie de la table 'Demande'");
});

const sql_create_R = `CREATE TABLE IF NOT EXISTS Reservation (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  id_utilisateur INTEGER,
  id_volontaire INTEGER,
  id_dispo INTEGER,
  FOREIGN KEY (id_utilisateur) REFERENCES Utilisateur(id_utilisateur),
  FOREIGN KEY (id_dispo) REFERENCES Disponibilites(id),
  FOREIGN KEY (id_volontaire) REFERENCES Utilisateur(id_utilisateur)
);`;

db.run(sql_create_R, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Création réussie de la table 'Reservation'");
});


app.set("view engine", "ejs");

app.set("views", path.join(__dirname, "views"));

app.use(express.static(path.join(__dirname, "views")));

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.use(express.urlencoded({ extended: false })); // <--- paramétrage du middleware

app.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true
}));

var bodyParser=require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var sessionData

app.listen(3000, () => {
  console.log("Serveur démarré (http://localhost:3000/) !");
});
/*Ecoute sur le port 3000*/

var domain ;
var nb_benevole =0;
var nb, villes;
var sess ;

/*Renvoi du header*/
app.get("/header", (req, res) => {
  console.log("header"+user);
   res.render("header" ,{ user_info: user });
});

/*Page index*/
app.get("/", (req, res) => {
 /*récupéretaion des domaines*/
  db.all("SELECT * FROM domaine;", [], (err, rows1) => {
     if (err) {
       return console.error(err.message);
     }
	  domain= rows1 ;
   });
   /*récupéretaion des utilisateurs*/
     db.all("SELECT * FROM Utilisateur;", [], (err, rows2) => {
     if (err) {
       return console.error(err.message);
     }
	  nb_benevole= rows2.length ;
   });
   /*récupéretaion des villes*/
  db.all("SELECT * FROM Ville;", [], (err, rows) => {
     if (err) {
       return console.error(err.message);
     }
	  nb= rows.length ;
	  villes = rows ;
    res.render("index" , { comp: comp_cherch,model: villes , nb_villes: nb, benevole: nb_benevole, domaine: domain, user_info: user, comp_exist :comp_exist } );
    comp_exist=0;
     });
});

app.get('/sign-up', function(req, res) {
  /*récupéretaion des villes*/
	 db.all("SELECT * FROM Ville;", [], (err, rows) => {
     if (err) {
       return console.error(err.message);
     }

     res.render("sign-up" , { model2: rows,message: message,user_info: user }  );
   });



});

app.get('/login', function(req, res) {

     res.render("login", { message: message_login, user_info: user } );

});

app.post("/loginService", (req, res)=> {

    var email = req.body.email;
    var  mdp = req.body.password;
    var mdp_db ='';
    var id ;
    /*récupération des données de l'utilisateur*/
  db.all('SELECT * FROM Utilisateur  where  email=?', [req.body.email], (err, rows) =>{
    rows.forEach(element => {
    mdp_db= element.mdp;
    id= element.id_utilisateur ;
    console.log(mdp_db);
  });

  if(rows.length!=0 && (mdp_db==mdp) ){
    /*Vérification du mdp*/
            req.session.user = id; //connection au compte utilisateur
            res.redirect('/profil');

         }
         else{
            message_login = 'Email ou Mot de passe incorrect!';
            console.log(message);
            res.render('login',{message: message_login, user_info: user});
         }

      });

});

// POST /Sign-up Ajouter utilisateur
app.post("/sign-up-service", (req, res) => {

  const sql = "INSERT INTO Utilisateur (nom, prenom, email,tel,mdp, role) VALUES (?, ?, ?, ?, ?, ?)";
  const user = [req.body.name, req.body.prenom, req.body.email, req.body.tel,req.body.password, 'USER'];

  db.all('SELECT email FROM Utilisateur WHERE email=?', [req.body.email], (err, rows) => {
	   if(rows.length){

		   message = "Cet email est déja utilisé par un autre utilisateur!! Essayer avec un autre";
		   res.redirect("/sign-up");
		   res.render('sign-up',{message: message,user_info: user});
	   }
	   else{
			 db.run(sql, user, err => {
				if (err) {
				   return console.error(err.message);
				 }
			  });
			  res.redirect("/login");
	   }
	});
});

// Update USer
// POST /modif des informations utilisateurs
app.post("/update-User", (req, res) => {
  /*requete pour la base de donnée*/
  const sql2 = "UPDATE  Utilisateur SET nom =?, prenom =?, email =?,tel =? WHERE id_utilisateur=?";
  const user_modified = [req.body.name, req.body.prenom, req.body.email, req.body.tel,req.session.user];
   db.run(sql2, user_modified, err => {
    if (err) {
           return console.error(err.message);
         }
         /*Redirection vers la page profil*/
        res.redirect('/profil');
   });
});



app.get('/profil', function(req, res) {
    /*Récupération des données utilisateurs et de ses compétences*/
     db.all("SELECT id_comp,date,meteo,horaire,nom,nom_comp,nom_domaine FROM Domaine,Disponibilites,Competences, Ville where  Disponibilites.id_utilisateur = ? AND Disponibilites.id_competence =Competences.id_comp and Competences.id_domaine=Domaine.id_domaine and Disponibilites.id_ville= Ville.id;", [req.session.user], (err, rowss) => {
         if (err) {
           return console.error(err.message);
         }
         else
         {
         rowss.forEach(element => console.log(element));
         dispo_user=rowss ;
       }
      });
     db.get("SELECT * FROM Utilisateur where id_utilisateur=?;", [req.session.user], (err, rows) => {
     if (err) {
       return console.error(err.message);
     }
     else {
        user =rows ;
        res.render("profil",{ user_info: user, dispo_user: dispo_user } );
     }
    });
});


app.get('/add_competence', function(req, res) {
  /*Ajouter une compétence au compte utilisteur*/
  db.all("SELECT * FROM Ville;", [], (err, rows) => {
     if (err) {
       return console.error(err.message);
     }
    villes = rows ;
  });

  db.all("SELECT * FROM domaine;", [], (err, rows1) => {
     if (err) {
       return console.error(err.message);
     }
    domain= rows1 ;
    res.render("add_competence" , { model: villes ,domaine: domain , user_info: user , errDate: err_date });
   });
 });

 app.post("/add_competence_service", function(req, res) {
   /*Récupérations des variables du formulaire*/
   var user = req.session.user;
   var domain = req.body.domain;
   var comp = req.body.competence;
   var date = req.body.date;
   var time = req.body.time;
   var meteo = "non";
   var ville = req.body.villes;
   err_date = null;

   if(domain == 1 || domain == 5){
     meteo = "oui";
   }

     var retdate = new Date();
     retdate.setDate(retdate.getDate());
     var mydate = new Date(date);
     var difference = mydate - retdate ;
     if (difference < 0){
       /*test sur la validité de la date*/
       err_date = "Veuillez entrer une date valide !!";
       res.redirect('/add_competence');
     }
     else {
      /*Ajout de la disponibilité au compte utilisateur*/
     const sql = "INSERT INTO Disponibilites (id_utilisateur,id_competence,horaire,date,meteo,id_ville) VALUES (?,?,?,?,?,?)";
     const dispo = [user,comp,time,date,meteo,ville];
     db.run(sql, dispo, err => {
      if (err) {
         return console.error(err.message);
       }
        res.redirect('/profil');
      });
    }
  });

app.get("/ask_service",function(req,res){
/*Page de demande de service*/
  db.all("SELECT * FROM Ville;", [], (err, rows) => {
     if (err) {
       return console.error(err.message);
     }
    villes = rows ;
  });

  db.all("SELECT * FROM domaine;", [], (err, rows1) => {
     if (err) {
       return console.error(err.message);
     }
    domain= rows1 ;
    res.render("ask_service" , { model: villes , domaine: domain , user_info: user , errDate : err_date});
   });

});

app.post("/ask_service", function(req, res) {

  var user = req.session.user;
  var domain = req.body.domain;
  var comp = req.body.competence;
  var date = req.body.date;
  var time = req.body.time;
  var meteo = "non";
  var ville = req.body.villes;
  err_date = null;

  if(domain == 1 || domain == 5){
    meteo = "oui";
  }

  var retdate = new Date();
  retdate.setDate(retdate.getDate());
  var mydate = new Date(date);
  var difference = mydate - retdate ;
  if (difference < 0){
    console.log("Mauvaise date" + retdate +" " + mydate);
    err_date = "Veuillez entrer une date valide !!";
    res.redirect('/ask_service');
  }
  else {
    const sql = "INSERT INTO Demande (id_utilisateur,id_competence,horaire,date,meteo,id_ville) VALUES (?,?,?,?,?,?)";
    const dem = [user,comp,time,date,meteo,ville];
    db.run(sql, dem, err => {
     if (err) {
        return console.error(err.message);
      }
       console.log("ajout demande");
       res.redirect('/profil');
     });
   }
 });

app.get('/logout',function(req,res){
    req.session.destroy(function(err) {
        if(err){
            message_logout = 'Error destroying session';
            res.json(message_logout);
        }else{
            message_logout = 'Session destroy successfully';
            user=null;
            return res.redirect("/");
            console.log(message_logout)
            res.json(message_logout);
        }
    });
});

app.get('/demandes',function(req,res){
  var dem_user ='';
  /*Visualisation des demandes de l'utilisateur*/
  db.all("SELECT Demande.id, id_comp,date,meteo,horaire,nom,nom_comp,nom_domaine FROM Domaine,Demande,Competences,Ville where  Demande.id_utilisateur = ? AND Demande.id_competence =Competences.id_comp and Competences.id_domaine=Domaine.id_domaine and Demande.id_ville= Ville.id;", [req.session.user], (err, rowss) => {
      if (err) {
    return console.error(err.message);
    console.log('err demande');
      }
      else {
      dem_user=rowss ;
    }
   });

  db.get("SELECT * FROM Utilisateur where id_utilisateur=?;", [req.session.user], (err, rows) => {
  if (err) {
    return console.error(err.message);
  }
  else {
     user =rows ;
     res.render("demandes",{ user_info: user, dem_info: dem_user } );
  }
});
});

app.get("/delete/:id", (req, res) => {
  /*suppréssion d'une compétence pour un utilisateur*/
  const id = req.params.id;
  console.log("delete"+id);
  const sql = "DELETE FROM Competences WHERE id_comp = ?";
  db.run(sql, id, err => {
    res.redirect("/profil");
  });
});


app.post("/chercher_competences", (req, res) => {
/*Recherche de compétence sur la page index*/
   const id = req.body.competence;
   const ville = req.body.ville_id;
   const sql = "SELECT  DISTINCT(Disponibilites.id),horaire, date, nom_comp ,Ville.nom as ville_name, Utilisateur.nom,prenom, Disponibilites.id_utilisateur FROM Ville,Disponibilites,Domaine,Utilisateur,Competences WHERE Disponibilites.id_competence=? and Ville.id=? and Utilisateur.id_utilisateur=Disponibilites.id_utilisateur and Competences.id_comp=Disponibilites.id_competence  and status=0";
    db.all(sql, [id,ville], (err, row) => {
      if (err) {
             return console.error(err.message);
           }
          comp_cherch=row ;
          comp_exist =1 ;
    res.redirect("/");
  });
});

var user_reservations =null;

app.get("/Reserver", (req, res) => {
  /*Service de réservation de compétence*/
  const id_dispo = req.query.comp;
  const id_user_dispo = req.query.user;
  const user_demandeur = req.session.user ;
  console.log("reserver"+id_dispo+ " " +id_user_dispo + "user " + user_demandeur);
  if(user_demandeur)
  {
    console.log("Connected");
    const sql = "INSERT INTO Reservation (id_utilisateur, id_volontaire, id_dispo) VALUES (?, ?, ?)";
    const sql2 = "UPDATE Disponibilites set status =1 where id = ?"
    const reservation = [user_demandeur,id_user_dispo,id_dispo];
    db.run(sql, reservation, err => {
        if (err) {
           return console.error(err.message);
         }
        });
     db.run(sql2, [id_dispo], err => {
        if (err) {
           return console.error(err.message);
         }
        });
     res.redirect("/");
  }
  else
  {
    message_login = 'Merci de vous Connecter pour pouvoir passer votre réservation';
    res.redirect("/login");
  }
});

app.get("/reservation", (req, res) => {
  /*Visualisation des réservations*/
  const sql = "select DISTINCT (Reservation.id),nom_comp, horaire,date, Utilisateur.nom,Utilisateur.prenom,Utilisateur.email,Utilisateur.tel FROM Disponibilites,Reservation,Utilisateur,Competences where Reservation.id_utilisateur=? and Reservation.id_volontaire=Disponibilites.id_utilisateur and Disponibilites.id_competence=Competences.id_comp and Utilisateur.id_utilisateur=Disponibilites.id_utilisateur and Reservation.id_dispo=Disponibilites.id;";
  db.all(sql, [req.session.user], (err, row) => {
      if (err) {
             return console.error(err.message);
           }
           user_reservations = row;
           res.render("reservation" ,{ user_reservations: user_reservations,user_info: user });
         });
});


app.get("/delete_Reservation/:id", (req, res) => {
  /*Suppréssion de réservation*/
  const id = req.params.id;
  const sql = "DELETE FROM Reservation WHERE id = ?";
  db.run(sql, id, err => {
    res.redirect("/reservation");
  });
});

app.get("/supprimer_compte/:id_utilisateur", (req, res) => {
  /*suppréssion de compte utilisateur*/
  const id = req.params.id_utilisateur;
  req.session.destroy();
  user=null;
  const sql = "DELETE FROM Utilisateur WHERE id_utilisateur = ?"
  db.run(sql, id, err => {
    res.redirect("/");
  });
});

app.get("/Annuler_demande/:id", (req, res) => {
  /*Annulation de demmande*/
  const id = req.params.id;
  console.log("La demande "+id+" est annulée");
  const sql = "DELETE FROM Demande  WHERE id = ?"
  db.run(sql, id, err => {
    res.redirect("/demandes");
  });
});
