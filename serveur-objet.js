const express = require("express");

const app = express();

const session = require ( 'express-session' ) ;

const path = require("path");

const sqlite3 = require("sqlite3").verbose();

const db_name = path.join(__dirname, "data", "datawebobjet.db");

var bodyParser = require('body-parser');

var ws = require("nodejs-websocket");

const router = express.Router();

/*Connexion à la base de donnée*/
const db = new sqlite3.Database(db_name, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Connexion réussie à la base de données 'datawebobjet.db'");
});

/*Création du serveur web objet*/
var server = ws.createServer(function(conn) {

var user = null;
    /*Message de connexion*/
    console.log("Nouvelle connexion");

    // Envoi d'un message texte
     conn.on("text", function(msg) {
        /*réception de l'ID utilisateur à partir de projet.ejs*/
       console.log("Texte reçu : " + msg);

        /*recherche des demmandes faites par l'utilisateur dans la bd*/
        db.all("SELECT * FROM Demande WHERE id_utilisateur = ?;", msg , (err, rows) => {
        if (err) {
          return console.error(err.message);
        }
        console.log("Demande");
        console.log(rows);
        /*Si des demandes existes*/
        if(rows[0] != null){
        /*Recherche des disponibilités correspondant à la demande*/
        db.all("SELECT  Disponibilites.id,Disponibilites.id_utilisateur,nom_comp,date,horaire,ville.nom as ville_name,Utilisateur.nom, Utilisateur.prenom FROM Disponibilites,Competences,Ville,Utilisateur WHERE id_competence = ? and id_ville = ? and horaire = ? and date = ? and status = 0 and Competences.id_comp=Disponibilites.id_competence and Utilisateur.id_utilisateur=Disponibilites.id_utilisateur and Ville.id=Disponibilites.id;", [rows[0].id_competence , rows[0].id_ville , rows[0].horaire, rows[0].date] , (err, rows1) => {
          if (err) {
            console.log(err.message);
          }
          else{
            if(rows1[0] != null){
            /*Si nous trouvons des correspondances*/
            console.log("Disponibilites de la demande approprie");
            /*Envoi des parametres de la disponibilité à la page profil.ejs*/
            conn.send(JSON.stringify(rows1[0]));
            console.log("Envoyé : "+rows1[0]);
          }
          }
        });
      }

      });
    });

    // Fermeture de connexion
    conn.on("close", function(code, reason) {
        console.log("Connexion fermée");
    });
    // En cas d'erreur
    conn.on("error", function(err) {
      console.log(err);
    });

}).listen(8001); // On écoute sur le port 8001
